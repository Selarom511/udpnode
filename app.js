const express = require('express');
const app = express();
const port = process.env.PORT || 3333;
const mongoose = require('mongoose');
const { mongoDbUrl } = require('./config/database');
const Vehiculo = require('./models/Vehiculo');

// Access-Control-Allow-Origin
// app.use((req, res, next) => {
//     res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
//     res.setHeader('Access-Control-Allow-Origin', 'http://172.16.6.27:4200');
//     res.setHeader('Access-Control-Allow-Origin', 'http://172.16.6.26:4200');
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     next();
// });

// Habilitando promesas para mongoose
mongoose.Promise = global.Promise;

// Conectando a mongodb
mongoose.connect(mongoDbUrl, { useNewUrlParser: true }).then(db => { console.log('MONGO connected') }).catch(err => console.log('Could not connect'));

app.get('/', (req, res) => {
    Vehiculo.count({}).then((data) => {
        console.log(`Data logged: ${data}`)
        Vehiculo.find({}).then(data => {
            res.status(202).send(data);
        })
    }).catch((err) => console.log(err));

});

app.listen(port, () => console.log(`Listening on port ${port}`));