const dgram = require('dgram');
const server = dgram.createSocket('udp4');
const mongoose = require('mongoose');
const { mongoDbUrl } = require('./config/database');
const Vehiculo = require('./models/Vehiculo');
const moment = require('moment');

// Habilitando promesas para mongoose
mongoose.Promise = global.Promise;

// Conectando a mongodb
mongoose.connect(mongoDbUrl, { useNewUrlParser: true }).then(db => { console.log('MONGO connected') }).catch(err => console.log('Could not connect'));

// Manejando error en server
server.on('error', (err) => {
    console.log(`server error:\n${err.stack}`);
    server.close();
});

// Capturando mensajes en server
server.on('message', (msg, rinfo) => {
    const message = msg.toString();
    const idxVin = message.indexOf('VIN=');
    const idxData = message.indexOf('#');
    const vin = message.slice(idxVin + 4);
    const data = message.slice(idxData + 1, idxVin - 1);
    Vehiculo.findOne({ vin: vin }).then((vehiculo) => {
            // console.log(vehiculo);
            // const fecha = new Date();
            if (!vehiculo) {
                vehiculo = new Vehiculo({
                    vin: vin,
                });
            }
            // vehiculo.registro.push({ data: data.split(","), fecha: moment().format('YYYY-MM-DD h:mm:ss a') });
            vehiculo.registro.push({ data: message, fecha: moment().format('YYYY-MM-DD h:mm:ss a') });
            vehiculo.save();
            console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
        })
        .catch((err) => console.log(err));
});

// escuchando desde el server
server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
});

// Configuración de server
server.bind({
    // address: 'localhost',
    port: 49156,
    exclusive: true
});